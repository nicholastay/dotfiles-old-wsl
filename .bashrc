alias v="vim"

# function to copy and v
templV() {
	cp $1 $2
	v $2
}

# random aliases
alias txa="templV ~/Template/math.tex"
alias txe="templV ~/Template/essay.md"

# config aliases
alias cfv="vim ~/.vimrc"
alias cfb="vim ~/.bashrc"


# windows bullshittery
windows() { [[ -n "$WINDIR" ]]; }
if windows; then
	source ~/.bash_windows
fi
