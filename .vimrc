" vimrc based on luke smith's

" leader
let mapleader =" "

" Pathogen
runtime bundle/vim-pathogen/autoload/pathogen.vim " non-default dir as submoduled
execute pathogen#infect()
execute pathogen#helptags()

set nocompatible
filetype plugin indent on
syntax on
set encoding=utf-8
set number
set relativenumber
set backspace=indent,eol,start

" .md is .markdown
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown'}

" line counts = f3
map <F3> :!wc <C-R>%<CR>

" spell-check = f6
map <F6> :setlocal spell! spelllang=en_au<CR>

" goyo (distraction-free mode)
map <F10> :Goyo<CR>
inoremap <F10> <esc>:Goyo<CR>a
" quit is quit
so ~/.vim/custom/goyo_quit.vim

set wildmode=longest,list,full
set wildmenu

" no trailing whitespace
autocmd BufWritePre * %s/\s\+$//e

" tabs are 4 chars wide
set tabstop=4
set softtabstop=0 noexpandtab
set shiftwidth=4

" auto indent
:set autoindent

" double space hacks
map <Space><Space> <Esc>/<++><Enter>"_c4l
inoremap <C-d> <Esc>/<++><Enter>"_c4l

" offline thesaurus
let g:tq_openoffice_en_file="~/other/dicts/th_en_AU_v2"
map <F9> :ThesaurusQueryReplaceCurrentWord<CR>

" some prose stuff
let g:pencil#wrapModeDefault = 'soft'
augroup pencil
	autocmd!
	autocmd FileType markdown call pencil#init()
	autocmd FileType text call pencil#init()
augroup END
let g:limelight_conceal_ctermfg = 'gray'
autocmd FileType markdown :Limelight
autocmd FileType text :Limelight
let g:wordy#ring = [
	\ 'weak',
	\ 'colloquial',
	\ 'contractions'
	\ ]
map <F12> :Wordy<space>weak

" ---------------------------------------------------------
"  BS OS stuff
if has("win32unix")
	let g:nick_templates = "$HOMEDRIVE$HOMEPATH\\\\Template"
	map <F7> :!sumatrapdf<space>%:r.pdf<space>&<CR><CR>
else
	let g:nick_templates = "~/Template"
	map <F7> :!mupdf<space>%:r.pdf<space>&<CR><CR>
endif


" ---------------------------------------------------------
"  Snippets

" LaTeX
" Compilation
autocmd FileType tex inoremap <F5> <Esc>:w<Enter>:!pdflatex<space><c-r>%<Enter><Enter>
autocmd FileType tex nnoremap <F5> :w<Enter>:!pdflatex<space><c-r>%<Enter><Enter>
" Code
autocmd FileType tex inoremap ;eq <Enter>\begin{equation*}<Enter><Enter>\end{equation*}<Enter><++><Esc>2ki<Tab>
autocmd FileType tex inoremap ;al <Enter>\begin{align*}<Enter><Enter>\end{align*}<Enter><++><Esc>2ki<Tab>
autocmd FileType tex inoremap ;eqn <Enter>\begin{align}<Enter><Enter>\end{align}<Enter><++><Esc>2ki<Tab>
autocmd FileType tex inoremap ;aln <Enter>\begin{align}<Enter><Enter>\end{align}<Enter><++><Esc>2ki<Tab>
autocmd FileType tex inoremap ;it \textit{}<++><Esc>T{i
autocmd FileType tex inoremap ;em \emph{}<++><Esc>T{i
autocmd FileType tex inoremap ;bf \textbf{}<++><Esc>T{i
autocmd FileType tex inoremap ;mx \begin{bmatrix*}<Enter><Enter><++><Esc>2k$i<Tab>
autocmd FileType tex inoremap ;chap \chapter{}<Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ;sec \section{}<Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ;ssec \subsection{}<Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ;sssec \subsubsection{}<Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ;tt \texttt{}<Space><++><Esc>T{i
autocmd FileType tex inoremap ;nc \newcommand{}[<++>]{<++>}<Esc>2T{i
autocmd FileType tex inoremap ;ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd FileType tex inoremap ;ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd FileType tex inoremap ;li <Enter>\item<Space>
autocmd FileType tex inoremap ;ref \ref{}<Space><++><Esc>T{i
autocmd FileType tex inoremap ;env \begin{}<Enter><++><Enter>\end{<++>}<Enter><++><Esc>3k$i
autocmd FileType tex inoremap <C-J> <space>\\<CR>

" Pandoc
" Compilation
autocmd FileType markdown nnoremap <F5> :w<Enter>:execute<space>'!pandoc --output=%:r.pdf --template='.g:nick_templates.'/pandoc_essay.tex --pdf-engine=xelatex %'<CR><CR>
autocmd FileType markdown nnoremap <S-F5> :w<Enter>:!pandoc --output=%:r.pdf -V geometry:"a4paper,margin=3cm" %<CR><CR>
autocmd FileType markdown nnoremap <C-F5> :w<Enter>:!pandoc --output=%:r.pdf -t beamer %<CR><CR>
